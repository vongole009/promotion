$(document).ready(function () {
    var table = $("#tbpromotion").DataTable({
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
    });

    $("#tbpromotion tbody").on("click", "td.details-control", function () {
        var tr = $(this).parents("tr");
        var row = table.row(tr);
        var column = tr.find(".details-control"); // this finds the HTML of the TD
        var obj = column.find(".btn.btn-requeue");

        var data = obj.val();
        //var myarr = data.split("|");
        //var id_payment = myarr[0];
        //var id_detail_payment = myarr[1];
        console.log(data);
        if (row.child.isShown()) {
            $(this).html(
                "<button type='button' class='btn btn-blue btn-xs waves-effect waves-light btn-requeue' value='" +
                    data +
                    "'><i class='fa fa-plus-circle'aria-hidden='true'></i></button>"
            );
            row.child.hide();
            tr.removeClass("shown");
        } else {
            row.child(
                "<div class='spinner-border spinner-border-sm m-2' role='status'></div>"
            ).show();
            $.ajax({
                method: "GET",
                url: "/admin/newpromotion/cb/datapromotion/" + data,
                /* data: {
                    promotionid: data,
                    _token: "{{ csrf_token() }}",
                },*/
                success: function (msg) {
                    row.child(msg).show();
                    tr.addClass("shown");
                },
            });
            $(this).html(
                "<button type='button' class='btn btn-blue btn-xs waves-effect waves-light btn-requeue' value='" +
                    data +
                    "'><i class='fa fa-minus-circle'aria-hidden='true'></i></button>"
            );
        }
    });
});
