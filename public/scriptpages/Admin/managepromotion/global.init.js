!(function (t) {
    "use strict";
    function e() {
        this.$body = t("body");
    }
    (e.prototype.init = function () {
        (Dropzone.autoDiscover = !1),
            t('[data-plugin="dropzone"]').each(function () {
                var e = t(this).attr("action"),
                    o = t(this).data("previewsContainer"),
                    i = { url: e };
                o && (i.previewsContainer = o);
                var r = t(this).data("uploadPreviewTemplate");
                r && (i.previewTemplate = t(r).html());
                t(this).dropzone(i);
            });
    }),
        (t.FileUpload = new e()),
        (t.FileUpload.Constructor = e);
})(window.jQuery),
    (function () {
        "use strict";
        window.jQuery.FileUpload.init();
    })(),
    0 < $('[data-plugins="dropify"]').length &&
        $('[data-plugins="dropify"]').dropify({
            messages: {
                default: "Drag and drop a file here or click",
                replace: "Drag and drop or click to replace",
                remove: "Remove",
                error: "Ooops, something wrong appended.",
            },
            error: { fileSize: "The file size is too big (5M max)." },
        });

$(document).ready(function () {
    $('[data-toggle="select2"]').select2(),
        $('[data-toggle="flatpicker"]').flatpickr({
            //altInput: !0,
            allowInput: true,
            altFormat: "F j, Y",
            dateFormat: "Y-m-d",
        });
});

function changelblbonusvaluetype(obj) {
    $("#divtypebonusvalue").show();
    var name = $(".typebonus_" + obj.value).html();
    $(".typebonus_lbl").html(name);
}

function callmaxrecrivepertype(obj) {
    $("#maxrecrivepertype").val(null);
    if (obj.value == "1") {
        $("#divmaxrecrivepertype").hide();
        $("#maxrecrivepertype").attr("required", false);
    } else {
        $("#divmaxrecrivepertype").show();
        $("#maxrecrivepertype").attr("required", true);
    }
}

function changenamelblturnovertypevalue(obj) {
    $("#divturnovertype_value").show();
    var name = $(".typeturnover_" + obj.value).html();
    $(".amountturnover").html(name);
}

function calltbadddeposit(obj) {
    $.ajax({
        url: "/admin/newpromotion/cb/tbadddeposit/" + obj.value,
        success: function (data) {
            $("#divtbadddeposit").html(data);
            //$('#modalorderedit').modal('show');
            //$("#blocker").hide();
        },
        dataType: "html",
    });
}

function addnewrow() {
    var $tableBody = $("#tbadddeposit").find("tbody"),
        $trLast = $tableBody.find("tr:last"),
        $trNew = $trLast.clone();

    $trLast.after($trNew);
}

function dellastrow() {
    //$("#tdtbadddeposit tr:not(:first)").remove();
    $("#tdtbadddeposit tr:not(:first):last").remove();
}

jQuery(document).ready(function () {
    var quill = new Quill("#snow-editor", {
        theme: "snow",
        modules: {
            toolbar: [
                //[{ font: [] }, { size: [] }],
                ["bold", "italic", "underline", "strike"],
                [{ color: [] }, { background: [] }],
                [{ script: "super" }, { script: "sub" }],
                [
                    { header: [!1, 1, 2, 3, 4, 5, 6] },
                    "blockquote",
                    //"code-block",
                ],
                [
                    { list: "ordered" },
                    { list: "bullet" },
                    { indent: "-1" },
                    { indent: "+1" },
                ],
                ["direction", { align: [] }],
                //["link", "image", "video"],
                ["clean"],
            ],
        },
    });
    $(".select2").select2({ width: "100%" });

    quill.on("text-change", function (delta, oldDelta, source) {
        document.getElementById("covertext").value = quill.root.innerHTML;
    });
});


