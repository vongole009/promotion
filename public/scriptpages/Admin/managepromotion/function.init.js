function confirmdelpromotion(obj) {
    swal({
        title: "ต้องการลบโปรโมชั่น?",
        text: "หากลบโปรโมชั่นนี้แล้ว จะไม่สามารถกู้คืนจากระบบได้!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            window.location = '/admin/delpromotion/' + obj.value
        }
    });
}
