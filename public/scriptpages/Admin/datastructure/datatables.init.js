$(document).ready(function () {
    var table = $("#tbsysbytedes").DataTable({
        searching: true,
        paging: true,
        ordering: false,
        info: true,
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>",
            },
        },
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass(
                "pagination-rounded"
            );
        },
        rowGroup: {
            dataSrc: [0],
        },
        columnDefs: [
            {
                targets: [0],
                visible: false,
            },
        ],
    });
});
