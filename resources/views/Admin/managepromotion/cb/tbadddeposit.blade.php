<table class="table table-bordered" style="width: 100%" id="tbadddeposit">
    <thead>
        <tr>
            <th style="vertical-align: middle;text-align:center;">ยอดฝากขึ้นต่ำ</th>
            @if ($typedeposit == '2' or $typedeposit == '3')
                <th style="vertical-align: middle;text-align:center;">ยอดฝากสูงสุด</th>
            @endif
            @if ($typedeposit == '3')
                <th style="vertical-align: middle;text-align:center;">ทำเทิร์นจำนวนเท่า</th>
                <th style="vertical-align: middle;text-align:center;">โบนัสสูงสุด</th>
            @endif

        </tr>
    </thead>
    <tbody id="tdtbadddeposit">
        <tr>
            <td><input type="number" class="form-control" name="mindeposit[]"></td>
            @if ($typedeposit == '2' or $typedeposit == '3')
                <td> <input type="number" class="form-control" name="maxdeposit[]"></td>
            @endif
            @if ($typedeposit == '3')
                <td> <input type="number" class="form-control" name="amountturn[]"></td>
                <td> <input type="number" class="form-control" name="maxbonus[]"></td>
            @endif
        </tr>
    </tbody>
    @if ($typedeposit == '3')
        <tfoot>
            <tr>
                <td colspan="4" align="center">
                    <button type="button" class="btn btn-blue  waves-effect waves-light" onclick="addnewrow();">
                        <i class="mdi mdi-bank-plus"></i> เพิ่ม
                    </button>
                    <button type="button" class="btn btn-danger waves-effect waves-light" onclick="dellastrow();">
                        <i class="mdi mdi-close"></i> ลบ
                    </button>
                </td>
            </tr>
        </tfoot>
    @endif
</table>
