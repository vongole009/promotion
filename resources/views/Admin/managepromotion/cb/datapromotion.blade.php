<div class="row">
    <div class="col-md-8">
        <div class="card ribbon-box">
            <div class="card-body">
                <div class="ribbon ribbon-info  float-start"><i class="mdi mdi-access-point me-1"></i> รายละเอียด</div>
                <div class="ribbon-content">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="header-title">ภาพหน้าปกโปรโมชั่น</h4>
                            <div class="bg-light">
                                <?php 
                                    $coverimage = asset($promotion->coverimage);
                                    ?>
                                <img src="{{ $coverimage }}" alt="product-pic" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4 class="header-title">ข้อมูลหน้าปก</h4>
                            {!! $promotion->covertext !!}
                        </div>
                       
                    </div>
                    <div class="mb-3" style="margin-top: 10px;">
                        <?php $tempkey = ''; ?>
                        @foreach ($bonustype as $key => $item)
                            <table class="table table-bordered dt-responsive nowrap w-100">
                                @if ($tempkey != $item->bonustypevalueid)
                                    <thead class="table-light">
                                        <tr>
                                            <th style="vertical-align: middle;text-align:center;">
                                                ยอดรับ{{ $item->sysbytedesname }}
                                            </th>
                                            <th style="vertical-align: middle;text-align:center;">รับโบนัสสูงสุด</th>
                                            <th style="vertical-align: middle;text-align:center;">ถอนได้สูงสุด</th>
                                        </tr>
                                    </thead>
                                @endif
                                <tbody>
                                    <tr>
                                        <td align="center">{{ $item->amountrecrived }}</td>
                                        <td align="right">{{ number_format($item->maxbonus, 2, '.', ',') }}</td>
                                        <td align="right">{{ number_format($item->maxwithdraw, 2, '.', ',') }}</td>
                                    </tr>
                                <tbody>
                            </table>
                        @endforeach
                        <h5 class="text-uppercase mt-0 mb-3 bg-light p-2">
                            <i class="fa fa-info-circle" aria-hidden="true"></i> กำหนดเงื่อนไข
                        </h5>

                        <div class="mb-3">
                            <table class="table table-bordered  dt-responsive nowrap w-100">
                                <thead class="table-light">
                                    <tr>
                                        <th style="vertical-align: middle;text-align:center;width: 30%;">รูปแบบรับโปร
                                        </th>
                                        <th style="vertical-align: middle;text-align:center;">
                                            จำนวนครั้งที่รับโปรโมชั่นได้
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($recrivepromotiontype as $item)
                                        <tr>
                                            <td>{{ $item->sysbytedesname }}</td>
                                            <td align="center">{{ $item->maxamountrecrive }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <table class="table table-bordered dt-responsive nowrap w-100">
                                <thead class="table-light">
                                    <tr>
                                        <th style="vertical-align: middle;text-align:center;width: 30%;">
                                            คำนวณทิร์นโอเวอร์
                                        </th>
                                        <th style="vertical-align: middle;text-align:center;">ยอดทำเทิร์น</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($turnovertype as $item)
                                        <tr>
                                            <td>{{ $item->sysbytedesname }}</td>
                                            <td align="center">{{ $item->amountturnover }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <table class="table table-bordered dt-responsive nowrap w-100">
                                <thead class="table-light">
                                    <tr>
                                        <th style="vertical-align: middle;text-align:center;width: 30%;">ประเภทยอดฝาก
                                        </th>
                                        <th style="vertical-align: middle;text-align:center;">ยอดฝากขึ้นต่ำ</th>
                                        <th style="vertical-align: middle;text-align:center;">ยอดฝากสูงสุด</th>
                                        <th style="vertical-align: middle;text-align:center;">ทำเทิร์นจำนวนเท่า</th>
                                        <th style="vertical-align: middle;text-align:center;">โบนัสสูงสุด</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($deposittype as $item)
                                        <tr>
                                            <td>{{ $item->sysbytedesname }}</td>
                                            <td align="right">{{ number_format($item->mindeposit, 2, '.', ',') }}</td>
                                            <td align="right">{{ number_format($item->maxdeposit, 2, '.', ',') }}</td>
                                            <td align="right">{{ number_format($item->amountturn, 2, '.', ',') }}</td>
                                            <td align="right">{{ number_format($item->maxbonus, 2, '.', ',') }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
