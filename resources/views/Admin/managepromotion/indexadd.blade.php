@extends('Template-Admin.master')
@section('tilte')
    เพิ่มรายการโปรโมชั่น
@endsection

@section('process')
    <?php
    $menu = 'menuadminmanagepromotion';
    $page = 'pageadminnewpromotion';
    ?>
@endsection

@section('pluginscss')
    <link href="{{ asset('themeadmin/assets/libs/quill/quill.core.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('themeadmin/assets/libs/quill/quill.bubble.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('themeadmin/assets/libs/quill/quill.snow.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item ">จัดการข้อมูลโปรโมชั่น</li>
                        <li class="breadcrumb-item active">เพิ่มรายการโปรโมชั่น</li>
                    </ol>
                </div>
                <h4 class="page-title"> เพิ่มรายการโปรโมชั่น </h4>
            </div>
        </div>
    </div>
    @if (session('msg'))
        <div class="col-md-12">
            <div class="card text-white bg-{{ session('alert') }} text-xs-center">
                <div class="card-body">
                    <h5 class="card-title">*** แจ้งเตือน ***</h5>
                    <p class="card-text">{{ session('msg') }}</p>
                </div>
            </div> <!-- end card-->
        </div>
    @endif
    <form action="/admin/newpromotion/addpromotion" method="post" data-plugin="dropzone"
        data-previews-container="#file-previews" data-upload-preview-template="#uploadPreviewTemplate"
        enctype="multipart/form-data".>
        {{ csrf_field() }}
        <div class="row">
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="text-uppercase mt-0 mb-3 bg-light p-2">ข้อมูลทั่วไป</h5>
                        <div class="row">
                            <div class="mb-3">
                                <label for="promotionname" class="form-label">ชื่อโปรโมชั่น <span
                                        style="color:red">*</span></label>
                                <input type="text" id="promotionname" name="promotionname" class="form-control"
                                    placeholder="ชื่อโปรโมชั่น" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <!-- Date View -->
                                <div class="mb-3">
                                    <!-- Date View -->
                                    <div class="mb-3">
                                        <label class="form-label">วันที่เริ่มใช้ <span style="color:red">*</span></label>
                                        <input type="text" class="form-control" data-toggle="flatpicker"
                                            name="startpromotion" placeholder="วันที่เริ่มใช้" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label class="form-label">สิ้นสุดวันที่ <span style="color:red">*</span></label>
                                    <input type="text" class="form-control" data-toggle="flatpicker" name="endpromotion"
                                        placeholder="สิ้นสุดวันที่" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-3">
                                <label for="product-description" class="form-label">รายละเอียด <span
                                        class="text-danger">*</span></label>
                                <div id="snow-editor" style="height: 150px;"></div>
                                <input type="hidden" id="covertext" name="covertext"></input>
                                <!-- end Snow-editor-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="text-uppercase bg-light p-2 mt-0 mb-3">ภาพหน้าปกโปรโมชั่น</h5>


                        <div class="my-3 mt-xl-0">
                            <label for="projectname" class="mb-0 form-label"></label>
                            <p class="text-muted font-14">ขนาดภาพที่แนะนำ 800x400 (px).</p>
                            <div class="col-lg-12">
                                <div class="mt-3">
                                    <input type="file" name="coverimage" data-plugins="dropify" data-max-file-size="5M"
                                        data-height="300" />
                                    <p class="text-muted text-center mt-2 mb-0">Max File size 5MB</p>
                                </div>
                            </div>
                            <!-- Preview -->
                            <div class="dropzone-previews mt-3" id="file-previews"></div>
                            <!-- file preview template -->
                            <div class="d-none" id="uploadPreviewTemplate">
                                <div class="card mt-1 mb-0 shadow-none border">
                                    <div class="p-2">
                                        <div class="row align-items-center">
                                            <div class="col-auto">
                                                <img data-dz-thumbnail="" src="#" class="avatar-sm rounded bg-light"
                                                    alt="">
                                            </div>
                                            <div class="col ps-0">
                                                <a href="javascript:void(0);" class="text-muted fw-bold"
                                                    data-dz-name=""></a>
                                                <p class="mb-0" data-dz-size=""></p>
                                            </div>
                                            <div class="col-auto">
                                                <!-- Button -->
                                                <a href="" class="btn btn-link btn-lg text-muted"
                                                    data-dz-remove="">
                                                    <i class="mdi mdi-close"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="text-uppercase mt-0 mb-3 bg-light p-2"> <i class="fa fa-info-circle"
                                aria-hidden="true"></i>
                            โบนัส</h5>
                        <div class="row">
                            <div class="mb-3">
                                <label class="form-label">คำนวณโบนัส <span style="color:red">*</span></label> <br>
                                @foreach ($typebonus as $item)
                                    <div class="form-check form-check-inline">
                                        <input type="radio" id="typebonus_{{ $item->sysbytedescode }}" name="typebonus"
                                            class="form-check-input" value="{{ $item->sysbytedescode }}"
                                            onclick="changelblbonusvaluetype(this)">
                                        <label class="form-check-label typebonus_{{ $item->sysbytedescode }}"
                                            for="typebonus_{{ $item->sysbytedescode }}">{{ $item->sysbytedesname }}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="row" style="display: none;" id="divtypebonusvalue">
                            <div class="col-lg-4">
                                <label class="form-label ">ยอดรับ<span class="typebonus_lbl"></span> <span
                                        style="color:red">*</span></label>
                                <input type="number" class="form-control" step="any" name="amountrecrived"
                                    min="0" value="" required>
                            </div>
                            <div class="col-lg-4">
                                <label class="form-label ">รับโบนัสสูงสุด <span style="color:red">*</span></label>
                                <input type="number" class="form-control" step="any" name="maxbonusbonusvalue"
                                    min="0" value="" required>
                            </div>
                            <div class="col-lg-4">
                                <label class="form-label ">ถอนได้สูงสุด <span style="color:red">*</span></label>
                                <input type="number" class="form-control" step="any" name="maxwithdraw"
                                    min="0" value="" required>
                            </div>
                        </div>
                        <hr>
                        <h5 class="text-uppercase mt-0 mb-3 bg-light p-2">
                            <i class="fa fa-info-circle" aria-hidden="true"></i> กำหนดเงื่อนไข
                        </h5>
                        <div class="row">
                            <div class="mb-3">
                                <label class="form-label">รูปแบบรับโปร</label><br>
                                @foreach ($typerecrivepromotion as $item)
                                    <div class="form-check form-check-inline">
                                        <input type="radio" id="typerecrivepromotion_{{ $item->sysbytedescode }}"
                                            name="{{ $item->columnanme }}" class="form-check-input"
                                            value="{{ $item->sysbytedescode }}" onclick="callmaxrecrivepertype(this);">
                                        <label class="form-check-label"
                                            for="typerecrivepromotion_{{ $item->sysbytedescode }}">{{ $item->sysbytedesname }}</label>
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-lg-4" style="display: none;" id="divmaxrecrivepertype">
                                <label class="form-label">จำนวนครั้งที่รับโปรโมชั่นได้</label> <br>
                                <input type="number" class="form-control" name="maxamountrecrive"
                                    id="maxrecrivepertype" value="" required>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="mb-3">
                                <label class="form-label">คำนวณทิร์นโอเวอร์</label> <br>
                                @foreach ($typeturnover as $item)
                                    <div class="form-check form-check-inline">
                                        <input type="radio" id="typeturnover_{{ $item->sysbytedescode }}"
                                            name="typeturnover" value="{{ $item->sysbytedescode }}"
                                            class="form-check-input" onclick="changenamelblturnovertypevalue(this)">
                                        <label class="form-check-label typeturnover_{{ $item->sysbytedescode }}"
                                            for="typeturnover_{{ $item->sysbytedescode }}">{{ $item->sysbytedesname }}</label>
                                    </div>
                                @endforeach

                            </div>
                        </div>

                        <div class="row" style="display: none" id="divturnovertype_value">
                            <div class="col-lg-4">
                                <label class="form-label amountturnover"></label>
                                <input type="number" class="form-control" name="amountturnover" step="any"
                                    min="0" value="">
                            </div>

                        </div>

                        <div class="row" style="margin-top: 10px;">
                            <div class="mb-3">
                                <label class="form-label">ยอดฝาก</label> <br>
                                @foreach ($typedeposit as $item)
                                    <div class="form-check form-check-inline">
                                        <input type="radio" id="typedeposit_{{ $item->sysbytedescode }}"
                                            name="typedeposit" class="form-check-input"
                                            value="{{ $item->sysbytedescode }}" onclick="calltbadddeposit(this);">
                                        <label class="form-check-label"
                                            for="typedeposit_{{ $item->sysbytedescode }}">{{ $item->sysbytedesname }}</label>
                                    </div>
                                @endforeach
                            </div>
                            <div class="mb-3" id="divtbadddeposit">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="text-center mb-3">
                        <button type="submit" class="btn btn-lg  btn-success waves-effect waves-light">
                            <i class="mdi mdi-content-save-outline"></i> บันทึกข้อมูล
                        </button>

                    </div>
                </div> <!-- end col -->
            </div>

        </div>
    </form>
    <div id="showmodal"></div>
@endsection

@section('pluginscript')
    <!-- Plugins js -->
    <script src="{{ asset('themeadmin/assets/libs/quill/quill.min.js') }}"></script>
@endsection

@section('script')
    <script src="{{ asset('scriptpages/Admin/managepromotion/datatables.init.js') }}"></script>
    <script src="{{ asset('scriptpages/Admin/managepromotion/global.init.js') }}"></script>
@endsection
