@extends('Template-Admin.master')
@section('tilte')
    รายการโปรโมชั่น
@endsection

@section('process')
    <?php
    $menu = 'menuadminmanagepromotion';
    $page = 'pageadminmanagepromotion';
    ?>
@endsection


@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">

                        <li class="breadcrumb-item active">รายการโปรโมชั่น</li>
                    </ol>
                </div>
                <h4 class="page-title"> รายการโปรโมชั่น </h4>
            </div>
        </div>
    </div>


    <div class="row col-md-12">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title"> โปรโมชั่น</h4>
                    <p class="text-muted font-13 mb-4">
                        จัดการข้อมูลโปรโมชั่น
                    </p>
                    <div class="row">
                        <div style="col-md-3">
                            <a href="/admin/newpromotion"class="btn btn-danger waves-effect waves-light">
                                <i class="mdi mdi-plus-circle me-1"></i> เพิ่มรายการโปรโมชั่น
                            </a>

                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
             
                        <table class="table table-bordered dt-responsive nowrap w-100" id="tbpromotion">
                            <thead>
                                <tr>
                                    <th style="vertical-align: middle;text-align:center;"></th>
                                    <th style="vertical-align: middle;text-align:center;">รายการโปรโมชั่น</th>
                                    <th style="vertical-align: middle;text-align:center;">วันที่เริ่ม</th>
                                    <th style="vertical-align: middle;text-align:center;">วันที่สิ้นสุด</th>
                                    <th style="vertical-align: middle;text-align:center;">สถานะ</th>
                                    <th style="vertical-align: middle;text-align:center;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($promotion as $item)
                                    <?php
                                    $statusonlinenme = $statusonline->where('sysbytedescode', $item->statusonline)->first()->sysbytedesname;
                                    $statusonlinebgcolor = $statusonline->where('sysbytedescode', $item->statusonline)->first()->bgcolor;
                                    ?>
                                    <tr>
                                        <td style="width: 1%;" class="details-control">
                                            <button type="button" class="btn btn-blue btn-xs waves-effect waves-light btn-requeue" value="{{ $item->promotionid }}">
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                            </button>
                                        </td>
                                        <td>{{ $item->promotionname }}</td>
                                        <td>{{ date_th_day($item->startpromotion) }}</td>
                                        <td>{{ date_th_day($item->endpromotion) }}</td>
                                        <td align="center">
                                            @if ($item->statusonline == '1')
                                                <i class=" far fa-check-circle" style="color: green;;"></i>
                                            @else
                                                <i class="far fa-times-circle" style="color: red;"></i>
                                            @endif
                                            {{ $statusonlinenme }}
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-outline-warning btn-xs waves-effect waves-light">
                                                <i class="far fa-edit"></i>
                                            </button>
                                            <button type="button" class="btn btn-outline-danger btn-xs waves-effect waves-light" value="{{ encrypt($item->promotionid) }}" onclick="confirmdelpromotion(this);">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="showmodal"></div>
@endsection

@section('script')
    <script src="{{ asset('scriptpages/Admin/managepromotion/datatables.init.js') }}"></script>
    <script src="{{ asset('scriptpages/Admin/managepromotion/function.init.js') }}"></script>
@endsection
