@extends('Template-Admin.master')
@section('tilte')
    โครงสร้างข้อมูล
@endsection

@section('process')
    <?php
    $menu = '';
    $page = '';
    ?>
@endsection


@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item active">จัดการข้อมูลพิ้นฐาน</li>
                        <li class="breadcrumb-item active">โครงสร้างข้อมูล</li>
                    </ol>
                </div>
                <h4 class="page-title"> โครงสร้างข้อมูล </h4>
            </div>
        </div>
    </div>


    <div class="row col-md-12">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title"> โครงสร้างข้อมูล</h4>
                    <p class="text-muted font-13 mb-4">
                        โครงสร้างข้อมูล
                    </p>
                    <div class="row" style="margin-top: 20px;">
                        <table class="table table-bordered dt-responsive nowrap w-100" id="tbsysbytedes">
                            <thead>
                                <tr>
                                    <th style="vertical-align: middle;text-align:center;">ชื่อตาราง</th>
                                    <th style="vertical-align: middle;text-align:center;">ชื่อ column</th>
                                    <th style="vertical-align: middle;text-align:center;">รหัส</th>
                                    <th style="vertical-align: middle;text-align:center;">ชื่อรหัส</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($sysbytedes as $item)
                                    <tr>
                                        <td >ชื่อตาราง : {{ $item->tablename }}</td>
                                        <td>{{ $item->columnanme }}</td>
                                        <td align="center">{{ $item->sysbytedescode }}</td>
                                        <td>{{ $item->sysbytedesname }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="showmodal"></div>
@endsection

@section('script')
    <script src="{{ asset('scriptpages/Admin/datastructure/datatables.init.js') }}"></script>
@endsection
