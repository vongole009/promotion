  <div class="scrollbar">
      <!--- Menu -->
      <ul class="menu">
          <li class="menu-title">รายการเมนู</li>
          <li class="menu-item">
              <a href="#menuadminmanagepromotion" data-bs-toggle="collapse" class="menu-link">
                  <span class="menu-icon"><i class="mdi mdi-ticket-confirmation-outline"></i></span>
                  <span class="menu-text"> จัดการโปรโมชั่น </span>
                  <span class="menu-arrow"></span>
              </a>
              <div class="collapse  collapse-md show " id="menuadminmanagepromotion">
                  <ul class="sub-menu">
                      <li class="menu-item ">
                          <a href="/admin" class="menu-link">
                              <span class="menu-text">รายการโปรโมชั่น</span>
                          </a>
                      </li>
                      <li class="menu-item ">
                          <a href="/admin/newpromotion" class="menu-link">
                              <span class="menu-text">เพิ่มรายการโปรโมชั่น</span>
                          </a>
                      </li>

                  </ul>
              </div>
          </li>

          <li class="menu-item">
            <a href="#menuTasks" data-bs-toggle="collapse" class="menu-link">
                <span class="menu-icon"><i class="mdi mdi-database-cog-outline"></i></span>
                <span class="menu-text"> จัดการข้อมูลพิ้นฐาน </span>
                <span class="menu-arrow"></span>
            </a>
            <div class="collapse" id="menuTasks">
                <ul class="sub-menu">
                    <li class="menu-item">
                        <a href="/admin/datastructure" class="menu-link">
                            <span class="menu-text">โครงสร้างข้อมูล</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>

      </ul>
      <!--- End Menu -->
      <div class="clearfix"></div>
  </div>
