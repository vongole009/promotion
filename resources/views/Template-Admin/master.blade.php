<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title> @yield('tilte')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('themeadmin/assets/images/favicon.ico') }}">

    <!-- Plugins css -->
    <link href="{{ asset('themeadmin/assets/libs/dropzone/min/dropzone.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('themeadmin/assets/libs/dropify/css/dropify.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('themeadmin/assets/libs/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('themeadmin/assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
    @yield('pluginscss')

    <!-- third party css -->
    <link href="{{ asset('themeadmin/assets/libs/datatables.net-bs5/css/dataTables.bootstrap5.min.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('themeadmin/assets/libs/datatables.net-responsive-bs5/css/responsive.bootstrap5.min.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('themeadmin/assets/libs/datatables.net-buttons-bs5/css/buttons.bootstrap5.min.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('themeadmin/assets/libs/datatables.net-select-bs5/css//select.bootstrap5.min.css') }}"
        rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('pluging/datatable/css/rowGroup.dataTables.min.css') }}" type="text/css">


    <!-- third party css end -->
    <!-- Theme Config Js -->
    <script src="{{ asset('themeadmin/assets/js/head.js') }}"></script>
    <!-- Bootstrap css -->
    <link href="{{ asset('themeadmin/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"
        id="app-style" />
    <!-- App css -->
    <link href="{{ asset('themeadmin/assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Icons css -->
    <link href="{{ asset('themeadmin/assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />

    <style>
        <style>.table tr td {
            font-size: 14.4px;
            padding-top: 0.5em;
            padding-bottom: 0.5em;
        }

        input.largerCheckbox {
            width: 15px;
            height: 15px;
            cursor: pointer;
            margin: auto;
            vertical-align: middle;
        }
    </style>


    <style>
        table.dataTable tr.dtrg-group.dtrg-level-0 td {
            font-weight: bold;
        }

        table.dataTable tr.dtrg-group td {
            background-color: #cdd8f6 !important;
        }

        table.dataTable tr.dtrg-group.dtrg-level-0 td:first-child {
            padding-left: 1em !important;
            padding-top: 0.5em !important;
            padding-bottom: 0.5em !important;
        }

        table.dataTable tr.dtrg-group.dtrg-level-1 td:first-child {
            padding-left: 2em !important;
        }

        table.dataTable tr.dtrg-group.dtrg-level-1 td {
            background-color: #d9ecf0 !important;
            padding-top: 0.5em !important;
            padding-bottom: 0.5em !important;
        }

        table.dataTable tr.dtrg-group.dtrg-level-2 td:first-child {
            padding-left: 4em !important;
        }

        table.dataTable tr.dtrg-group.dtrg-level-2 td {
            background-color: #fcf3dd !important;
            padding-top: 0.5em !important;
            padding-bottom: 0.5em !important;
        }

        table.dataTable tr.dtrg-group.dtrg-level-3 td:first-child {
            padding-left: 6em !important;
        }

        table.dataTable tr.dtrg-group.dtrg-level-3 td {
            background-color: #edf3e9 !important;
            padding-top: 0.5em !important;
            padding-bottom: 0.5em !important;
            font-size: 14.4px !important;
        }

        table.dataTable tr.dtrg-group.dtrg-level-4 td:first-child {
            padding-left: 8em !important;
        }

        table.dataTable tr.dtrg-group.dtrg-level-4 td {
            background-color: #ebebee !important;
            padding-top: 0.5em !important;
            padding-bottom: 0.5em !important;
            font-size: 14.4px !important;
        }

        table.dataTable tr.dtrg-group.dtrg-level-5 td:first-child {
            padding-left: 10em !important;
        }

        table.dataTable tr.dtrg-group.dtrg-level-5 td {
            background-color: #fae2ee !important;
            padding-top: 0.5em !important;
            padding-bottom: 0.5em !important;
            font-size: 14.4px !important;
        }

        table.dataTable td:first-child {
            padding-left: 2em !important;
        }

        table.dataTable td {
            padding-top: 0.5em !important;
            padding-bottom: 0.5em !important;
            font-size: 14.4px !important;
        }

       

     
    </style>

</head>

<body>
    @yield('process')

    <!-- Begin page -->
    <div id="wrapper">

        <!-- Pre-loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">Loading...</div>
            </div>
        </div>
        <!-- End Preloader-->

        <!-- ========== Menu ========== -->
        <div class="app-menu">

            <!-- Brand Logo -->
            <div class="logo-box">
                <!-- Brand Logo Light -->
                <a href="#" class="logo-light">
                    <i class="fas fa-ad"></i></i> Promotion
                </a>

                <!-- Brand Logo Dark -->
                <a href="index.html" class="logo-dark">
                    <i class="fas fa-ad"></i> Promotion
                </a>
            </div>

            <!-- menu-left -->
            @include('Template-Admin.menu')
        </div>
        <!-- ========== Left menu End ========== -->





        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">

            <!-- ========== Topbar Start ========== -->
            <div class="navbar-custom">
                @include('Template-Admin.topbar')
            </div>
            <!-- ========== Topbar End ========== -->

            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">

                    @yield('content')

                </div> <!-- container -->

            </div> <!-- content -->

            <!-- Footer Start -->
            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div>
                                <script>
                                    document.write(new Date().getFullYear())
                                </script> © Promotion - <a href="#" target="_blank">Promotion</a>
                            </div>
                        </div>
                        <!--<div class="col-md-6">
                                <div class="d-none d-md-flex gap-4 align-item-center justify-content-md-end footer-links">
                                    <a href="javascript: void(0);">About</a>
                                    <a href="javascript: void(0);">Support</a>
                                    <a href="javascript: void(0);">Contact Us</a>
                                </div>
                            </div>-->
                    </div>
                </div>
            </footer>
            <!-- end Footer -->

        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->

    <!-- Theme Settings -->
    <div class="offcanvas offcanvas-end right-bar" tabindex="-1" id="theme-settings-offcanvas">
        <div class="d-flex align-items-center w-100 p-0 offcanvas-header">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-bordered nav-justified w-100" role="tablist">
                <li class="nav-item">
                    <a class="nav-link py-2" data-bs-toggle="tab" href="#chat-tab" role="tab">
                        <i class="mdi mdi-message-text d-block font-22 my-1"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link py-2" data-bs-toggle="tab" href="#tasks-tab" role="tab">
                        <i class="mdi mdi-format-list-checkbox d-block font-22 my-1"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link py-2 active" data-bs-toggle="tab" href="#settings-tab" role="tab">
                        <i class="mdi mdi-cog-outline d-block font-22 my-1"></i>
                    </a>
                </li>
            </ul>
        </div>



        <div class="offcanvas-footer border-top py-2 px-2 text-center">
            <div class="d-flex gap-2">
                <button type="button" class="btn btn-light w-50" id="reset-layout">Reset</button>
                <a href="https://1.envato.market/uboldadmin" class="btn btn-danger w-50" target="_blank"><i
                        class="mdi mdi-basket me-1"></i> Buy</a>
            </div>
        </div>
    </div>

    <!-- Vendor js -->
    <script src="{{ asset('themeadmin/assets/js/vendor.min.js') }}"></script>

    <!-- App js -->
    <script src="{{ asset('themeadmin/assets/js/app.min.js') }}"></script>

    <!-- third party js -->
    <script src="{{ asset('themeadmin/assets/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('themeadmin/assets/libs/datatables.net-bs5/js/dataTables.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('themeadmin/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('themeadmin/assets/libs/datatables.net-responsive-bs5/js/responsive.bootstrap5.min.js') }}">
    </script>
    <script src="{{ asset('themeadmin/assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('themeadmin/assets/libs/datatables.net-buttons-bs5/js/buttons.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('themeadmin/assets/libs/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('themeadmin/assets/libs/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('themeadmin/assets/libs/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('themeadmin/assets/libs/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('themeadmin/assets/libs/datatables.net-select/js/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('pluging/datatable/js/dataTables.rowGroup.min.js') }}"></script>

    <script src="{{ asset('themeadmin/assets/libs/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('themeadmin/assets/libs/pdfmake/build/vfs_fonts.js') }}"></script>
    <!-- third party js ends -->

    <!-- plugin js -->
    <script src="{{ asset('themeadmin/assets/libs/dropzone/min/dropzone.min.js') }}"></script>
    <script src="{{ asset('themeadmin/assets/libs/dropify/js/dropify.min.js') }}"></script>

    <script src="{{ asset('themeadmin/assets/libs/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('themeadmin/assets/libs/flatpickr/flatpickr.min.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    @yield('pluginscript')

    @yield('script')

</body>

</html>
