<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin', "managepromotionController@index");
Route::get('/admin/newpromotion', "managepromotionController@indexaddpromotion");
Route::post('/admin/newpromotion/addpromotion', "managepromotionController@addpromotion");
Route::get('/admin/delpromotion/{promotionid}', "managepromotionController@delpromotion");


Route::get('/admin/newpromotion/cb/tbadddeposit/{typedeposit}', "managepromotionController@tbadddeposit");
Route::get('/admin/newpromotion/cb/datapromotion/{promotionid}', "managepromotionController@datapromotion");

Route::get('/admin/datastructure', "datastructureController@index");