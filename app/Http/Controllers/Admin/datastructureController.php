<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Collection;

class datastructureController extends Controller
{
    public function index()
    {
        $sysbytedes = DB::table('sysbytedes')->orderBy("tablename", 'asc')->get();

        return view("Admin.datastructure.index",[
            "sysbytedes" => $sysbytedes
        ]);
    }
}
