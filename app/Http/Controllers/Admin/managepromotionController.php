<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class managepromotionController extends Controller
{
    public function index()
    {
        $promotion = DB::table('promotion')->whereNull("deleted")->get();

        $statusonline = DB::table('sysbytedes')->whereNull("deleted")
            ->where("columnanme", "statusonline")
            ->where("tablename", "promotion")->get();
        $statusonline = new Collection($statusonline);

        return view("Admin.managepromotion.index", [
            "promotion" => $promotion,
            "statusonline" => $statusonline
        ]);
    }

    public function indexaddpromotion()
    {
        $typebonus = DB::table('sysbytedes')->whereNull("deleted")
            ->where("columnanme", "typebonus")
            ->where("tablename", "bonustype")->get();

        $bonusvaluetype = DB::table('sysbytedes')->whereNull("deleted")
            ->where("columnanme", "value")
            ->where("tablename", "bonustype")->get();

        $typerecrivepromotion = DB::table('sysbytedes')->whereNull("deleted")
            ->where("columnanme", "typerecrivepromotion")
            ->where("tablename", "recrivepromotiontype")->get();

        $typeturnover = DB::table('sysbytedes')->whereNull("deleted")
            ->where("columnanme", "typeturnover")
            ->where("tablename", "turnovertype")->get();

        $typedeposit = DB::table('sysbytedes')->whereNull("deleted")
            ->where("columnanme", "typedeposit")
            ->where("tablename", "deposittype")->get();




        return view("Admin.managepromotion.indexadd", [
            "typebonus" => $typebonus,
            "bonusvaluetype" => $bonusvaluetype,
            "typerecrivepromotion" => $typerecrivepromotion,
            "typeturnover" => $typeturnover,
            "typedeposit" => $typedeposit
        ]);
    }

    public function tbadddeposit($typedeposit)
    {
        return view("Admin.managepromotion.cb.tbadddeposit", [
            "typedeposit" => $typedeposit
        ]);
    }

    public function addpromotion(Request $request)
    {
        try {
            //ข้อมูลทั่วไป
            $promotionname = $request->input("promotionname");
            $startpromotion = $request->input("startpromotion");
            $endpromotion = $request->input("endpromotion");
            $covertext = $request->input("covertext");

            $statusonline = 0;
            $promotionid = DB::table('promotion')->insertGetId([
                "promotionname" => $promotionname,
                "startpromotion" => $startpromotion,
                "endpromotion" => $endpromotion,
                "statusonline" => $statusonline,
                "covertext" => $covertext,
            ]);
            //ภาพหน้าปกโปรโมชั่น
            $coverimage = $request->input("coverimage");

            //dump($request->all());
            $set_flodername = "promotion/promotionid_" . $promotionid;
            if ($request->hasFile('coverimage')) {

                $key = 1;
                $uploadedFile = $request->file('coverimage');
                $array = explode('.', $uploadedFile->getClientOriginalName());
                $file_type = end($array);
                $filename = "$promotionid" . "_$key." . strtolower($file_type);

                Storage::disk('local')->putFileAs(
                    $set_flodername,
                    $uploadedFile,
                    $filename
                );
                $path = $set_flodername . '/' . $filename;
                DB::table('promotion')->where("promotionid", $promotionid)->update([
                    "coverimage" => $path
                ]);
            }

            $conditionid = DB::table('condition')->insertGetId([
                "promotionid" => $promotionid,
            ]);

            //คำนวณโบนัส
            $typebonus = $request->input("typebonus");
            $amountrecrived = $request->input("amountrecrived");
            $maxbonusbonusvalue = $request->input("maxbonusbonusvalue");
            $maxwithdraw = $request->input("maxwithdraw");

            $bonustypeid = DB::table('bonustype')->insertGetId([
                "conditionid" => $conditionid,
                "typebonus" => $typebonus,
            ]);


            DB::table('bonustypevalue')->insert([
                "bonustypeid" => $bonustypeid,
                "amountrecrived" => $amountrecrived,
                "maxbonus" => $maxbonusbonusvalue,
                "maxwithdraw" => $maxwithdraw
            ]);


            //รูปแบบรับโปร
            $typerecrivepromotion = $request->input("typerecrivepromotion");
            $maxamountrecrive = $request->input("maxamountrecrive");
            if ($maxamountrecrive == "") {
                $maxamountrecrive = "0";
            }

            $recrivepromotiontypeid = DB::table('recrivepromotiontype')->insertGetId([
                "conditionid" => $conditionid,
                "typerecrivepromotion" => $typerecrivepromotion,
            ]);

            DB::table('recrivepromotiontypevalue')->insert([
                "recrivepromotiontypeid" => $recrivepromotiontypeid,
                "maxamountrecrive" => "$maxamountrecrive",

            ]);
            //echo "<pre>" . print_r($maxamountrecrive, true) . "</pre>";

            //คำนวณทิร์นโอเวอร์
            $typeturnover = $request->input("typeturnover");
            $amountturnover = $request->input("amountturnover");

            $turnovertypeid = DB::table('turnovertype')->insertGetId([
                "conditionid" => $conditionid,
                "typeturnover" => $typeturnover,
            ]);

            DB::table('turnovertypevalue')->insertGetId([
                "turnovertypeid" => $turnovertypeid,
                "amountturnover" => $amountturnover,
            ]);

            //ยอดฝาก
            $typedeposit = $request->input("typedeposit");

            $deposittypeid = DB::table('deposittype')->insertGetId([
                "conditionid" => $conditionid,
                "typedeposit" => $typedeposit,
            ]);
            //echo "<pre>" . print_r($typedeposit, true) . "</pre>";
            $depostitypevalue = array();
            foreach ($request->input("mindeposit") as $key => $mindeposit) {

                $maxdeposit = 0;
                $amountturn = 0;

                if (isset($request->input("maxdeposit")[$key])) {
                    $maxdeposit = $request->input("maxdeposit")[$key];
                }
                if (isset($request->input("amountturn")[$key])) {
                    $amountturn = $request->input("amountturn")[$key];
                }

                $depostitypevalue = array(
                    "deposittypeid" => $deposittypeid,
                    "mindeposit" => $mindeposit,
                    "maxdeposit" => $maxdeposit,
                    "amountturn" => $amountturn
                );
            }

            DB::table('depostitypevalue')->insert($depostitypevalue);

            return redirect()->back()->with([
                'msg' => 'บันทึกข้อมูลเรียบร้อย',
                'alert' => 'success '
            ]);
        } catch (\Throwable $th) {
            $msg =  $th->getMessage();
            return redirect()->back()->with([
                'msg' => $msg,
                'alert' => 'danger'
            ]);
        }
    }

    public function delpromotion($promotionid)
    {
        try {
            $promotionid = decrypt($promotionid);

            DB::table('promotion')->where("promotionid", $promotionid)->update([
                "deleted" => now()
            ]);

            return redirect()->back()->with([
                'msg' => 'บันทึกข้อมูลเรียบร้อย',
                'alert' => 'success '
            ]);
        } catch (\Throwable $th) {
            $msg =  $th->getMessage();
            return redirect()->back()->with([
                'msg' => $msg,
                'alert' => 'danger'
            ]);
        }
    }

    public function datapromotion($promotionid)
    {
        $promotion = DB::table('promotion')->where("promotionid", $promotionid)->get()[0];
        $condition = DB::table('condition')->whereNull("deleted")
            ->where("promotionid", $promotionid)->get();

        $listconditionid = "";
        foreach ($condition as $key => $item) {
            if ($listconditionid == "") {
                $listconditionid = $item->conditionid;
            } else {
                $listconditionid .= ',' . $item->conditionid;
            }
        }


        $strsql = "SELECT bonustype.bonustypeid,
        bonustype.conditionid,
        bonustype.typebonus,
        bonustypevalue.bonustypevalueid,
        bonustypevalue.amountrecrived,
        bonustypevalue.maxbonus,
        bonustypevalue.maxwithdraw,
        sysbytedes.sysbytedesname
        FROM bonustype
        INNER JOIN bonustypevalue ON bonustype.bonustypeid = bonustypevalue.bonustypeid
        INNER JOIN (SELECT * FROM sysbytedes WHERE  sysbytedes.columnanme = 'typebonus' AND sysbytedes.tablename = 'bonustype') AS sysbytedes ON bonustype.typebonus = sysbytedes.sysbytedescode
        WHERE bonustype.deleted IS NULL
        AND bonustype.conditionid IN($listconditionid)";
        $bonustype = DB::select($strsql);
        //echo "<pre>" . print_r($strsql, true) . "</pre>";

        $strsql = "SELECT recrivepromotiontype.recrivepromotiontypeid,
        recrivepromotiontype.conditionid,
        recrivepromotiontype.typerecrivepromotion,
        sysbytedes.sysbytedesname,
        recrivepromotiontypevalue.recrivepromotiontypevalueid,
        recrivepromotiontypevalue.maxamountrecrive
        FROM recrivepromotiontype
        INNER JOIN recrivepromotiontypevalue ON recrivepromotiontype.recrivepromotiontypeid = recrivepromotiontypevalue.recrivepromotiontypeid
        INNER JOIN (SELECT * FROM sysbytedes WHERE  sysbytedes.columnanme = 'typerecrivepromotion' AND sysbytedes.tablename = 'recrivepromotiontype') AS sysbytedes ON recrivepromotiontype.typerecrivepromotion = sysbytedes.sysbytedescode
        WHERE recrivepromotiontype.deleted IS NULL
        AND recrivepromotiontype.conditionid IN($listconditionid)";
        $recrivepromotiontype = DB::select($strsql);

        $strsql = "SELECT turnovertype.turnovertypeid,
        turnovertype.conditionid,
        turnovertype.typeturnover,
        sysbytedes.sysbytedesname,
        turnovertypevalue.turnovertypevalueid,
        turnovertypevalue.amountturnover
        FROM turnovertype
        INNER JOIN turnovertypevalue ON turnovertype.turnovertypeid = turnovertypevalue.turnovertypeid
        INNER JOIN (SELECT * FROM sysbytedes WHERE  sysbytedes.columnanme = 'typeturnover' AND sysbytedes.tablename = 'turnovertype') AS sysbytedes ON turnovertype.typeturnover = sysbytedes.sysbytedescode
        WHERE turnovertype.deleted IS NULL
        AND turnovertype.conditionid IN ($listconditionid)";
        $turnovertype = DB::select($strsql);

        $strsql = "SELECT deposittype.deposittypeid,
        deposittype.conditionid,
        deposittype.typedeposit,
        sysbytedes.sysbytedesname,
        depostitypevalue.depostitypevalueid,
        depostitypevalue.mindeposit,
        depostitypevalue.maxdeposit,
        depostitypevalue.amountturn,
        depostitypevalue.maxbonus
        FROM deposittype
        INNER JOIN depostitypevalue ON deposittype.deposittypeid = depostitypevalue.deposittypeid
        INNER JOIN (SELECT * FROM sysbytedes WHERE  sysbytedes.columnanme = 'typedeposit' AND sysbytedes.tablename = 'deposittype') AS sysbytedes ON deposittype.typedeposit = sysbytedes.sysbytedescode
        WHERE deposittype.deleted IS NULL
        AND deposittype.conditionid IN ($listconditionid)";
        $deposittype = DB::select($strsql);

        return view("Admin.managepromotion.cb.datapromotion", [
            "promotion" => $promotion,
            "bonustype" => $bonustype,
            "recrivepromotiontype" => $recrivepromotiontype,
            "turnovertype" => $turnovertype,
            "deposittype" => $deposittype
        ]);
    }
}
