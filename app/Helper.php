<?php

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

if (!function_exists('cal_classtime')) {
    function cal_classtime($seconds)
    {
        $t = round($seconds);
        return sprintf('%02d:%02d:%02d', ($t / 3600), ($t / 60 % 60), $t % 60);
    }
}

if (!function_exists('date_th')) {
    function date_th($next)
    {
        if ($next != "") {
            $xxx = explode("-", $next);
            $y = $xxx[0] + 543;
            $m = $xxx[1] + 0;

            $time = "";
            $d = explode(" ", $xxx[2]);
            if (count($d) > 1) {
                $time = $d[1];
            }

            $d = str_pad($d[0], 2, "0", STR_PAD_LEFT);

            $mth[1] = "ม.ค.";
            $mth[2] = "ก.พ.";
            $mth[3] = "มี.ค.";
            $mth[4] = "เม.ย.";
            $mth[5] = "พ.ค.";
            $mth[6] = "มิ.ย.";
            $mth[7] = "ก.ค.";
            $mth[8] = "ส.ค.";
            $mth[9] = "ก.ย.";
            $mth[10] = "ต.ค.";
            $mth[11] = "พ.ย.";
            $mth[12] = "ธ.ค.";

            $di = "$d $mth[$m] $y $time";

            if ($y == "0000" or $m == "00" or $m == "0" or $d == "00" or $d == "0") {
                return "-";
            } else {
                return $di;
            }
        } else {
            return "-";
        }
    }
}

if (!function_exists('date_th_day')) {
    function date_th_day($next)
    {
        if ($next != "") {
            $xxx = explode("-", $next);
            $y = $xxx[0] + 543;
            $m = $xxx[1] + 0;

            $time = "";
            $d = explode(" ", $xxx[2]);
            if (count($d) > 1) {
                $time = $d[1];
            }

            $d = str_pad($d[0], 2, "0", STR_PAD_LEFT);

            $mth[1] = "ม.ค.";
            $mth[2] = "ก.พ.";
            $mth[3] = "มี.ค.";
            $mth[4] = "เม.ย.";
            $mth[5] = "พ.ค.";
            $mth[6] = "มิ.ย.";
            $mth[7] = "ก.ค.";
            $mth[8] = "ส.ค.";
            $mth[9] = "ก.ย.";
            $mth[10] = "ต.ค.";
            $mth[11] = "พ.ย.";
            $mth[12] = "ธ.ค.";

            $di = "$d $mth[$m] $y";

            if ($y == "0000" or $m == "00" or $m == "0" or $d == "00" or $d == "0") {
                return "-";
            } else {
                return $di;
            }
        } else {
            return "";
        }
    }
}



if (!function_exists('ENGLIST')) {
    function ENGLIST($NO)
    {
        $ENGLIST = array(
            "1" => 'A',
            "2" => 'B',
            "3" => 'C',
            "4" => 'D',
            "5" => 'E',
            "6" => 'F',
            "7" => 'G',
            "8" => 'H',
            "9" => 'I',
            "10" => 'J',
            "11" => 'K',
            "12" => 'L',
            "13" => 'M',
            "14" => 'N',
            "15" => 'O',
            "16" => 'P',
            "17" => 'Q',
            "18" => 'R',
            "19" => 'S',
            "20" => 'T',
            "0" => 'U',
            "21" => 'V',
            "22" => 'W',
            "23" => 'X',
            "24" => 'Y',
            "25" => 'Z'
        );
        return $ENGLIST[$NO];
    }
}

if (!function_exists('differDayOfDateTime')) {
    function differDayOfDateTime($datetimeFirst, $datetimeSecond)
    {
        $timestampFirst = strtotime($datetimeFirst);
        $timestampSecond = strtotime($datetimeSecond);
        if ($timestampFirst <= $timestampSecond) {
            $timestampDiffer = abs($timestampFirst - $timestampSecond);
            $numberOfDay = $timestampDiffer / (60 * 60 * 24);
        } else {
            $numberOfDay = 0;
        }
        $timestampDiffer = abs($timestampFirst - $timestampSecond);
        return $numberOfDay;
    }
}



function convert($number)
{
    $txtnum1 = array('ศูนย์', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า', 'สิบ');
    $txtnum2 = array('', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน');
    $number = str_replace(",", "", $number);
    $number = str_replace(" ", "", $number);
    $number = str_replace("บาท", "", $number);
    $number = explode(".", $number);
    if (sizeof($number) > 2) {
        return 'ปรับให้ ทศนิยม ไม่เกิน 2 ตำแหน่ง';
        exit;
    }
    $strlen = strlen($number[0]);
    $convert = '';


    for ($i = 0; $i < $strlen; $i++) {
        $n = substr($number[0], $i, 1);


        if ($n != 0) {
            if ($strlen == 1) {
                $convert .= $txtnum1[$n];
            } else {
                if ($i == ($strlen - 1) and $n == 1) {
                    $convert .= 'เอ็ด';
                } elseif ($i == ($strlen - 2) and $n == 2) {
                    $convert .= 'ยี่';
                } elseif ($i == ($strlen - 2) and $n == 1) {
                    $convert .= '';
                } else {
                    $convert .= $txtnum1[$n];
                }
                $convert .= $txtnum2[$strlen - $i - 1];
            }
        }
    }

    $convert .= 'บาท';
    if (
        $number[1] == '0' or $number[1] == '00' or
        $number[1] == ''
    ) {
        $convert .= 'ถ้วน';
    } else {
        $strlen = strlen($number[1]);
        for ($i = 0; $i < $strlen; $i++) {
            $n = substr($number[1], $i, 1);
            if ($n != 0) {
                if ($i == ($strlen - 1) and $n == 1) {
                    $convert
                        .= 'เอ็ด';
                } elseif (
                    $i == ($strlen - 2) and
                    $n == 2
                ) {
                    $convert .= 'ยี่';
                } elseif (
                    $i == ($strlen - 2) and
                    $n == 1
                ) {
                    $convert .= '';
                } else {
                    $convert .= $txtnum1[$n];
                }
                $convert .= $txtnum2[$strlen - $i - 1];
            }
        }
        $convert .= 'สตางค์';
    }
    return $convert;
}


if (!function_exists('date_th_time')) {
    function date_th_time($next)
    {
        if ($next != "") {
            $xxx = explode("-", $next);
            $y = $xxx[0] + 543;
            $m = $xxx[1] + 0;

            $time = "";
            $d = explode(" ", $xxx[2]);
            if (count($d) > 1) {
                $time = $d[1];
                return $time;
            } else {
                return "-";
            }
        } else {
            return "-";
        }
    }
}
